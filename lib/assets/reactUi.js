window.ReactUJS = {
    CLASS_NAME_ATTR: 'data-react-class',

    getElements: function getElements() {
        var finder = function finder(selector) {
            return typeof jQuery === 'undefined' ? document.querySelectorAll(selector) : jQuery(selector);
        };

        return finder('[' + ReactUJS.CLASS_NAME_ATTR + ']');
    },

    mountComponents: function mountComponents() {
        var element, reactClass, props;

        var elements = ReactUJS.getElements();

        var index = function index(obj, i) {
            return obj[i];
        };

        elements.forEach(function (element) {
            reactClass = element.getAttribute(ReactUJS.CLASS_NAME_ATTR).split('.').reduce(index, window);
            props = JSON.parse(element.getAttribute(ReactUJS.PROPS_ATTR));

            ReactDOM.render(React.createElement(reactClass, props), element);
        });

    },

    unmountComponents: function unmountComponents() {
        var elements = ReactUJS.getElements();

        elements.forEach(function (element) {
            ReactDOM.unmountComponentAtNode(element);
        });
    },

    handleEvents: function handleEvents() {
        document.readyState === "complete" || document.readyState === "loaded" || document.readyState === "interactive" ?
            ReactUJS.mountComponents() : document.addEventListener('DOMContentLoaded', ReactUJS.mountComponents);

        window.addEventListener('unload', ReactUJS.unmountComponents);
    }
};

ReactUJS.handleEvents();