<?php

namespace bTokman\react\widgets;

use ReactJS;
use Yii;
use yii\base\Widget;
use yii\web\NotFoundHttpException;
use bTokman\react\ReactUiAsset;

/**
 * Class ReactRenderer - yii2 widget to server-side react rendering
 * and implementing it on client side.
 * This widget require V8JS PHP extension: http://php.net/v8js
 * @package bTokman\react\widgets
 */
class ReactRenderer extends Widget
{
    /**
     * Path to your React components js file
     * @var string
     */
    public $componentsSourceJs;

    /**
     * Component name
     * @var string
     */
    public $component;

    /**
     * React component props
     * @var array
     */
    public $props;

    /**
     * Options for html attributes
     * @var array
     */
    public $options = [];

    /**
     * Instance of ReactJS object
     * More information https://github.com/reactjs/react-php-v8js
     * @var  ReactJS
     */
    private $_react;

    /**
     *  Default options, "tag" - place to render react app - default to "div"
     * "prerender" - tell to widget render your react app on server side - default to true
     * @var  array
     */
    private $_defaultOptions = [
        'tag' => 'div',
        'prerender' => true
    ];

    /**
     * Initializes the widget.
     */
    public function init()
    {
        if (empty($this->componentsSourceJs) || !file_exists($this->componentsSourceJs)) {
            throw new NotFoundHttpException('React component source js file doesn\'t exist');
        }
        /*** Get ReactJs server side render instance */
        $this->_react = new ReactJS($this->getReactSource(), $this->getSourceJs());

        parent::init();
    }

    /**
     * Runs the widget.
     */
    public function run()
    {
        $this->applyJs();
        return $this->renderReact();
    }

    /**
     * Apply js in view
     */
    public function applyJs()
    {
        $this->getView()->registerJsFile($this->componentsSourceJs);
        ReactUiAsset::register($this->view);
    }

    /**
     * Rendering function
     * @return string
     */
    public function renderReact()
    {
        $options = array_merge($this->_defaultOptions, $this->options);
        $tag = $options['tag'];
        $markup = '';

        if ($options['prerender'] === true) {
            $markup = $this->_react->setComponent($this->component, $this->props)->getMarkup();
        }

        $props = htmlentities(json_encode($this->props), ENT_QUOTES);

        $htmlAttributes = array_diff_key($options, $this->_defaultOptions);
        $htmlAttributesString = $this->arrayToHTMLAttributes($htmlAttributes);

        $this->registerProps($props);

        return "<{$tag} data-react-class='{$this->component}'{$htmlAttributesString}>{$markup}</{$tag}>";
    }


    /**
     * Register component props
     * @param $props
     */
    protected function registerProps($props)
    {
        $this->getView()->registerJs("window.{$this->component} = $props");
    }

    /**
     * Convert associative array to string of HTML attributes
     * @param $array
     * @return string
     */
    private function arrayToHTMLAttributes($array)
    {
        $htmlAttributesString = '';
        foreach ($array as $attribute => $value) {
            $htmlAttributesString .= "{$attribute} = '{$value}'";
        }
        return $htmlAttributesString;
    }

    /**
     * Get source component js for ReactJs constructor
     * @return string
     */
    private function getSourceJs()
    {
        return file_get_contents($this->componentsSourceJs);
    }

}